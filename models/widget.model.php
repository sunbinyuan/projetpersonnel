<?php
require_once dirname(__FILE__) . "/index.php";

Class Widget extends BasicModel {
  protected $table = "widgets";

  public function add($userid) {
    $result = $this->connect()->prepare("INSERT INTO {$this->table} (name, content, displayorder, enabled, userid) VALUES ('Nouveau widget', '', 1, 1, :userid)");
    $result->bindParam(':userid', $userid);
    if ($result->execute()) {
      return true;
    } else {
      throw new Exception("Une erreur s'est produite lors de l'ajout du widget. Veuillez réssayer plus tard.", 1);
      return false;
    }    
  }

  public function save($array) {
    if (!isset($array['id']) || !isset($array['userid'])) {
      throw new Exception("Une erreur s'est produite lors de la sauvegarde des changements. Veuillez réssayer plus tard.", 1);
      return false;
    } else {
      $result = $this->connect()->prepare("UPDATE {$this->table} SET enabled = :enabled, displayorder = :displayorder WHERE id = :id AND userid = :userid");
      $result->bindParam(':enabled', $enabled);
      $result->bindParam(':displayorder', $displayorder);
      $result->bindParam(':id', $id);
      $result->bindParam(':userid', $array['userid']);
      foreach ($array['id'] as $id => $value) {
        $enabled = $value['enabled'];
        $displayorder = $value['displayorder'];

        if(!$result->execute()) {
          throw new Exception("Une erreur s'est produite lors de la sauvegarde de votre style. Veuillez réssayer plus tard.", 1);
          return false;
        }
      }
      return true;
    }
  }

  public function remove ($array) {
    if (!isset($array['id']) || !isset($array['userid'])) {
      return false;
    } else {
      $result = $this->connect()->prepare("DELETE FROM {$this->table} WHERE userid = :userid AND id = :id");
      $result->bindParam(':userid', $array['userid']);
      $result->bindParam(':id', $id);
      foreach ($array['id'] as $key => $id) {
        if(!$result->execute()) {
          throw new Exception("Une erreur s'est produite lors de la création de votre compte. Veuillez réssayer plus tard.", 1);
          return false;
        }
      }
      return true;
    }
  }

  public function count($userid) {
    $result = $this->connect()->prepare("SELECT null FROM {$this->table} WHERE userid = :userid");
    $result->bindParam(':userid', $userid);
    $result->execute();
    return $result->rowCount();
  }

  public function modify($array) {
    if (!isset($array['id']) || !isset($array['userid']) || !isset($array['name']) || !isset($array['content'])) {
      throw new Exception("Une erreur s'est produite lors de la sauvegarde de votre widget. Veuillez réssayer plus tard.", 1);
      return false;
    } else {
      $result = $this->connect()->prepare("UPDATE {$this->table} SET name = :name, content = :content WHERE id = :id AND userid = :userid");
      if ($result->execute(
        array(
          ':id' => $array['id'],
          ':userid' => $array['userid'],
          ':name' => $array['name'],
          ':content' => $array['content'],
        )
      ))
      {
        return true;
      } else {
        throw new Exception("Une erreur s'est produite lors de la sauvegarde de votre widget. Veuillez réssayer plus tard.", 1);
        return false;
      }
    }
  }

  public function check ($array) {
    $result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE id = :id AND userid = :userid");
    if ($result->execute(
      array(
        'id' => $array['id'],
        ':userid' => $array['userid'],
      )
    ))
    {
      if ($result->rowCount() > 0)  {
        return true;
      } else {
        return false;
      }
    } else {
      throw new Exception("Une erreur s'est produite lors de la sauvegarde de votre style. Veuillez réssayer plus tard.", 1);
      return false;
    }
  }

  public function widgetFetch() {
    if (func_num_args() % 4 === 0 || (func_num_args() + 1) % 4 === 0) {
      $params = array();
      $arg_list = func_get_args();
      $query = null;
      for ($i=0; $i < func_num_args(); $i++) { 
        if ($i % 4 == 0) {
          $query .= $arg_list[$i] . " ";
        } elseif (($i - 1) % 4 == 0) {
          $isOperator = in_array($arg_list[$i], $this->operators);
          if($isOperator) {
            $query .= $arg_list[$i] . " ";
          } else {
            throw new Exception("Error Processing Request", 1);
          }
        } elseif (($i - 2) % 4 == 0) {
          array_push($params, $arg_list[$i]);
          $query .= "? ";
        } elseif (($i - 3) % 4 == 0) {
          $query .= $arg_list[$i] . " ";
        }
      }
      $result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE $query");
      $result->execute($params);
      return (($result->rowCount() > 0)? $result->fetchAll() : false);
    } else {
      throw new Exception("Error Processing Request", 1);
      
    }

  }
}