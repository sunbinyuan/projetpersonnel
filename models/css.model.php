<?php
require_once dirname(__FILE__) . "/index.php";

Class CSS extends BasicModel {
	protected $table = "css";

	public function add($userid) {
    $newcss = file_get_contents(dirname(__FILE__) . '/../css/blog_style.css', FILE_USE_INCLUDE_PATH);
    $result = $this->connect()->prepare("INSERT INTO {$this->table} (name, css, selected, userid) VALUES ('Nouveau style', :css, '0', :userid)");
	  $result->bindParam(':css', $newcss);
  	$result->bindParam(':userid', $userid);
    if ($result->execute()) {
    	return true;
    } else {
    	throw new Exception("Une erreur s'est produite lors de l'ajout du style. Veuillez réssayer plus tard.", 1);
    	return false;
    }    
  }

	public function select ($array) {
		if (isset($array['id']) && isset($array['userid'])) {
			$result = $this->connect()->prepare("UPDATE {$this->table} SET selected = '0' WHERE userid = :userid");
	  	$result->bindParam(':userid', $array['userid']);
	  	$result->execute();
	    $result = $this->connect()->prepare("UPDATE {$this->table} SET selected = '1' WHERE id = :id");
	    $result->bindParam(':id', $array['id']);
	    $result->execute();
		}
	}

	public function remove ($array) {
		if (!isset($array['id']) || !isset($array['userid'])) {
			return false;
		} else {
	    $result = $this->connect()->prepare("DELETE FROM {$this->table} WHERE userid = :userid AND id = :id");
  	  $result->bindParam(':userid', $array['userid']);
    	$result->bindParam(':id', $array['id']);
      if ($result->execute()) {
      	return true;
      } else {
      	throw new Exception("Une erreur s'est produite lors de la création de votre compte. Veuillez réssayer plus tard.", 1);
      	return false;
      }
  	}
	}

	public function count($userid) {
    $result = $this->connect()->prepare("SELECT null FROM {$this->table} WHERE userid = :userid");
    $result->bindParam(':userid', $userid);
    $result->execute();
    return $result->rowCount();
	}

	public function modify($array) {
 // $title = strip_tags($array['title']);

		if (!isset($array['id']) || !isset($array['css']) || !isset($array['name']) || !isset($array['userid'])) {
	  	throw new Exception("Une erreur s'est produite lors de la sauvegarde de votre style. Veuillez réssayer plus tard.", 1);
			return false;
		} else {
		  $result = $this->connect()->prepare("UPDATE {$this->table} SET name = :name, css = :css WHERE id = :id AND userid = :userid");
		  if ($result->execute(
		    array(
		    	':id' => $array['id'],
					':userid' => $array['userid'],
		      ':name' => $array['name'],
		      ':css' => $array['css'],
		    )
		  ))
		  {
		  	return true;
		  } else {
		  	throw new Exception("Une erreur s'est produite lors de la sauvegarde de votre style. Veuillez réssayer plus tard.", 1);
		  	return false;
		  }
		}
	}

	public function check ($array) {
		$result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE id = :id AND userid = :userid");
	  if ($result->execute(
	    array(
	    	'id' => $array['id'],
	      ':userid' => $array['userid'],
	    )
	  ))
	  {
	  	if ($result->rowCount() > 0)  {
	  		return true;
	  	} else {
	  		return false;
	  	}
	  } else {
	  	throw new Exception("Une erreur s'est produite lors de la sauvegarde de votre style. Veuillez réssayer plus tard.", 1);
	  	return false;
	  }
	}

	public function getCSS() {
		if (func_num_args() % 4 === 0 || (func_num_args() + 1) % 4 === 0) {
			$params = array();
			$arg_list = func_get_args();
			$query = null;
			for ($i=0; $i < func_num_args(); $i++) { 
				if ($i % 4 == 0) {
					$query .= $arg_list[$i] . " ";
				} elseif (($i - 1) % 4 == 0) {
					$isOperator = in_array($arg_list[$i], $this->operators);
					if($isOperator) {
						$query .= $arg_list[$i] . " ";
					} else {
						throw new Exception("Error Processing Request", 1);
					}
				} elseif (($i - 2) % 4 == 0) {
					array_push($params, $arg_list[$i]);
					$query .= "? ";
				} elseif (($i - 3) % 4 == 0) {
					$query .= $arg_list[$i] . " ";
				}
			}
			$result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE $query");
			$result->execute($params);
			return (($result->rowCount() > 0)? $result->fetchAll() : false);
		} else {
			throw new Exception("Error Processing Request", 1);
			
		}

	}
}