<?php
require_once dirname(__FILE__) . "/index.php";

Class Blog extends BasicModel {
	protected $table = "blogs";

	function getUserByBlog ($blogid) {
		$result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE id = :blogid");
	  $result->bindParam(':blogid', $blogid);
		$result->execute();
		$row = $result->fetch(PDO::FETCH_OBJ);
		$userid = $row->userid;
		return $userid;
	}
	function getBlogByUser ($userid) {
		$result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE userid = :userid");
	  $result->bindParam(':userid', $userid);
		$result->execute();
		$row = $result->fetch(PDO::FETCH_OBJ);
		$blogid = $row->id;
		return $blogid;
	}

	public function check ($userid) {
		$result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE userid = :userid");
		$result->bindParam(':userid', $userid);
	  if ($result->execute()) {
	  	if ($result->rowCount() > 0) {
	  		return true;
	  	} else {
	  		return false;
	  	}
	  } else {
	  	throw new Exception("Une erreur s'est produite. Veuillez réssayer plus tard.", 1);
	  	return false;
	  }
	}

	public function add ($array) {
		if (!isset($array['name']) || !isset($array['userid'])) {
			return false;
		} else {
	    $result = $this->connect()->prepare("INSERT INTO {$this->table} (name, userid) VALUES (:name, :userid)");
	    $result->bindParam(':name', $array['name']);
	    $result->bindParam(':userid', $array['userid']);
	    if($result->execute()) {
	    	return true;
	    } else {
	    	return false;
	    }
  	}
	}
	public function blogChange ($array) {
		if (!isset($array['name']) || !isset($array['userid'])) {
			return false;
		} else {
	    $result = $this->connect()->prepare("UPDATE {$this->table} SET name = :name WHERE userid = :userid");
	    $result->bindParam(':name', $array['name']);
	    $result->bindParam(':userid', $array['userid']);
	    if($result->execute()) {
	    	return true;
	    } else {
	    	return false;
	    }
  	}
	}
	public function remove ($userid) {
		if (!isset($userid)) {
			return false;
		} else {
	    $result = $this->connect()->prepare("DELETE FROM {$this->table} WHERE userid = :userid");
	    $result->bindParam(':userid', $userid);
	    if($result->execute()) {
	    	return true;
	    } else {
	    	return false;
	    }
  	}
	}

	public function lists () {
	    $result = $this->connect()->prepare("SELECT * FROM {$this->table} ORDER BY name");
	    if($result->execute()) {
	    	return $result->fetchAll();
	    } else {
	    	return false;
	    }
  	}

}