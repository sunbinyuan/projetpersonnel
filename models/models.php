<?php
//IoC and Facade
require_once dirname(__FILE__) . "/facade.php";

//CSS Model
require_once dirname(__FILE__) . "/css.model.php";
IoC::set('CSS', 'CSS');
class fCSS extends Facade {
    protected static function getName() { return 'CSS'; }
}
//Users Model
require_once dirname(__FILE__) . "/user.model.php";
IoC::set('User', 'User');
class fUser extends Facade {
    protected static function getName() { return 'User'; }
}
//Blogs Model
require_once dirname(__FILE__) . "/blog.model.php";
IoC::set('Blog', 'Blog');
class fBlog extends Facade {
    protected static function getName() { return 'Blog'; }
}
//Posts Model
require_once dirname(__FILE__) . "/post.model.php";
IoC::set('Post', 'Post');
class fPost extends Facade {
    protected static function getName() { return 'Post'; }
}
//Widgets Model
require_once dirname(__FILE__) . "/widget.model.php";
IoC::set('Widget', 'Widget');
class fWidget extends Facade {
    protected static function getName() { return 'Widget'; }
}