<?php
require_once dirname(__FILE__) . "/index.php";

Class Post extends BasicModel {
	protected $table = "posts";

	function getBlogByPost($postid) {
		$result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE id = :postid");
	  $result->bindParam(':postid', $postid);
		$result->execute();
		$row = $result->fetch(PDO::FETCH_OBJ);
		$blogid = $row->blogid;
		return $blogid;
	}

	function getPosts($array) {
		if (!isset($array['blogid']) || (isset($array['index']) Xor isset($array['limit']))) {
			throw new Exception("Arguments missing.", 1);
		} else {
			$limit = ((isset($array['index']) AND isset($array['blogid'])) ? "LIMIT {$array['index']}, {$array['limit']}" : "");
			$result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE blogid = :blogid ORDER BY publishdate DESC $limit");
		  $result->bindParam(':blogid', $array['blogid']);
			$result->execute();
			return $result->fetchAll();
		}
	}

	function count($blogid) {
		if (!$blogid) {
			throw new Exception("Arguments missing.", 1);
		} else {
			$result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE blogid = :blogid");
		  $result->bindParam(':blogid', $blogid);
			$result->execute();
			return $result->rowCount();
		}
	}

	function save ($array) {
		if (!isset($array['title']) || !isset($array['content']) || (!isset($array['blogid']) && !isset($array['postid']))) {
			//If the array is missing
			return false;
		} else {
			if (isset($array['postid']) && !isset($array['blogid'])) {
        $result = $this->connect()->prepare("UPDATE {$this->table} SET title = :title, content = :content, publishdate = :publishdate WHERE id = :postid");
        $result->bindParam(':postid', $array['postid']);
		    $result->bindParam(':publishdate', $array['publishdate']);
			} elseif (!isset($array['postid']) && isset($array['blogid'])) {
				$result = $this->connect()->prepare("INSERT INTO {$this->table} (title, publishdate, content, blogid) VALUES (:title, :time, :content, :blogid)");
			  $result->bindParam(':blogid', $array['blogid']);
		    $result->bindParam(':time', time());

			} else {
				throw new Exception("Choose either PostID or BlogID.", 1);
				return false;
			}
		  $result->bindParam(':title', $array['title']);
			$result->bindParam(':content', $array['content']);
	    if ($result->execute()) {
      	return true;
      } else {
      	throw new Exception("Une erreur s'est produite lors de l'envoie de l'entrée. Veuillez réssayer plus tard.", 1);
      	return false;
      }
	  }
	}

	public function remove ($array) {
		if (!isset($array['id']) || !isset($array['blogid'])) {
			return false;
		} else {
	    $result = $this->connect()->prepare("DELETE FROM {$this->table} WHERE blogid = :blogid AND id = :id");
  	  $result->bindParam(':blogid', $array['blogid']);
    	$result->bindParam(':id', $array['id']);
      if ($result->execute()) {
      	return true;
      } else {
      	throw new Exception("Une erreur s'est produite lors de la création de votre compte. Veuillez réssayer plus tard.", 1);
      	return false;
      }
  	}
	}
}