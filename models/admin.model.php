<?php
require_once dirname(__FILE__) . "/index.php";

Class Admin extends BasicModel {
  protected $table = "users";

  public function register($array) {
    if (!isset($array['username']) || !isset($array['password']) || !isset($array['email'])) {
      //If the array is missing
      return false;
    } else {
      $fields = implode(", ", array_keys($array));
      $values = rtrim(str_repeat("?, ", count($array)), ", ");
      $execute = array_values($array);
      $result = $this->connect()->prepare("INSERT INTO {$this->table} ($fields) VALUES ($values)");
      if ($result->execute($execute)) {
          // Send Email
          $to  = $array['email']; // note the comma
          
          // subject
          $subject = 'Verification de votre email \'' . $array['email'] . '\'';
          
          // message
          $message = '
            Hey, je voudrais m\'assurer que vous êtes bel et bien \'' . $array['username'] . '\'. Valider cette adresse va vous donner access à tout le site. Si vous désirez continuer, suivez le lien ci-dessous:

            http://' . SITENAME . '/activate/' . $array['code'] . '

            Si vous n\'êtes pas '. $array['username'] .' ou que ce message vous a été envoyé par erreur, vous pouvez ignorer ce message.
          ';
          
          // To send HTML mail, the Content-type header must be set
          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/plain; charset=utf-8' . "\r\n";
          
          // Additional headers
          $headers .= 'To:' .  $array['email'] . "\r\n";
          $headers .= 'From: Projetpersonnel.tk <sunbinyuan@projetpersonnel.tk>' . "\r\n";
          //$headers .= 'Cc: info@lbacreations.com' . "\r\n";
          
          // Mail it
          mail($to, $subject, $message, $headers);
          session_start();
          $_SESSION["global"] = 'Un email d\'activation a été envoyé.';
        return true;
      } else {
        throw new Exception("Une erreur s'est produite lors de la création de votre compte. Veuillez réssayer plus tard.", 1);
        return false;
      }
    }
  }

  public function login($array) {
    session_start();
    if (!isset($array['username']) || !isset($array['password'])) {
      return false;
    } else {
      $result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE username = :username AND password = :password");
      $result->bindParam(':username', $array['username']);
      $result->bindParam(':password', $array['password']);
      $result->execute();
      if ($result->rowCount() > 0) {
        $data = $result->fetch(PDO::FETCH_ASSOC);
        if ($data['active'] == 0) {
          return false;
        }
        //Get user IP Address
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $update = $this->connect()->prepare("UPDATE {$this->table} SET ip = :ip, lastlogin = :lastlogin WHERE username = :username");
        $update->bindParam(':ip', $ip);
        $update->bindParam(':lastlogin', time());
        $update->bindParam(':username', $array['username']);
        if ($update->execute()) {
          for ($i=0; $i < count($data); $i++) { 
            $_SESSION[array_keys($data)[$i]] = $data[array_keys($data)[$i]];
          }
          return true;
        } else {
          $_SESSION['login'] = "Une erreur est survenue durant la connexion. Veuillez réssayer plus tard.";
          return false;
        }
          
      } else {
        $_SESSION['login'] = "L'utilisateur ou le mot de passe est invalide.";  
        return false;
      }
    }
  }
  public function changepass($array) {
    session_start();
    $result = $this->connect()->prepare("UPDATE {$this->table} SET password = :password WHERE username = :username");
    $result->bindParam(':password', $array['password']);
    $result->bindParam(':username', $array['username']);
    if ($result->execute()) {
      return true;
    } else {
      return false;
    }
  }

  public function resetresetcode($username) {
    $result = $this->connect()->prepare("UPDATE {$this->table} SET resetcode = NULL WHERE username = :username");
    $result->bindParam(':username', $username);
    return $result->execute();
  }
  public function reset($array) {
    session_start();
    $result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE email = :email");
    $result->bindParam(':email', $array['email']);
    if ($result->execute()) {
      if ($result->rowCount() > 0) {
        $row = $result->fetch(PDO::FETCH_OBJ);
        $update = $this->connect()->prepare("UPDATE {$this->table} SET resetcode = :resetcode WHERE email = :email");
        $update->bindParam(':resetcode', $array['resetcode']);
        $update->bindParam(':email', $array['email']);
        if ($update->execute()) {

          // Send Email
          $to  = $array['email']; // note the comma
          
          // subject
          $subject = 'Réinitialisation de votre mot de passe';
          
          // message
          $message = '
            J\'ai entendu que vous avez perdu votre mot de passe. Désolé à propos de ça!

            Mais ne vous inquiétez pas! Vous pouvez utilser le lien suivant pour réinitialiser votre mot de passe.

            http://' . SITENAME . '/reset/' . $array['resetcode'] . '
          ';
          
          // To send HTML mail, the Content-type header must be set
          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/plain; charset=utf-8' . "\r\n";
          
          // Additional headers
          $headers .= 'To:' .  $array['email'] . "\r\n";
          $headers .= 'From: Projetpersonnel.tk <sunbinyuan@projetpersonnel.tk>' . "\r\n";
          //$headers .= 'Cc: info@lbacreations.com' . "\r\n";
          
          // Mail it
          mail($to, $subject, $message, $headers);

          $_SESSION['global'] = "Un email a été envoyé à votre adresse courriel.";  
          return true;
        } else {
          $_SESSION['reset'] = "Une erreur est survenue durant la connexion. Veuillez réssayer plus tard."; 
          return false;
        }
      } else {
          $_SESSION['reset'] = "Cette adresse courriel n'est lié à aucun compte. Vérifiez l'orthographe.";  
          return false;
      }
    } else {
      $_SESSION['reset'] = "Une erreur est survenue durant la connexion. Veuillez réssayer plus tard."; 
      return false;
    }
  }
  public function logout() {
    session_start();
    session_destroy();
  }

  public function checkuser($username) {
    $result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE username = :user");
    $result->bindParam(':user', $username);
    $result->execute();
    return (($result->rowCount() > 0)? true : false);
  }

  public function activate($username) {
    $result = $this->connect()->prepare("UPDATE {$this->table} SET active = 1, code = NULL WHERE username = :user");
    $result->bindParam(':user', $username);
    return $result->execute();
  }

  public function checkemail($email) {
    $result = $this->connect()->prepare("SELECT * FROM {$this->table} WHERE email = :email");
    $result->bindParam(':email', $email);
    $result->execute();
    return (($result->rowCount() > 0)? true : false);
  }
}