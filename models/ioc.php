<?
class IoC {

    private static $instances = array();

    public static function set($name, $instance)
    {
        if (is_string($instance)) {
            $instance = new $instance();
        }

        static::$instances[$name] = $instance;
    }

    public static function get($name)
    {
        $instance = static::$instances[$name];

        if ($instance instanceof Closure) {
            $instance = $instance();
        }

        return $instance;
    }

}