<?php 
  session_start(); 
  ob_start();
  require_once ('models/models.php');
  require_once ('config/vars.php');
  require_once ('config/functions.php');
  require_once ('libs/disqus.php');
  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="projet personnel">
    <meta name="author" content="Binyuan Sun">
    <link rel="shortcut icon" href="/favicon.ico">
    <title>%TITLE%<?php echo SITENAME; ?></title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- Custom styles for this template -->
    <link href="/css/default_style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><?php echo SITESNAME; ?></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Journal de bord <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <?php 
                  $result = fBlog::lists();
                  foreach ($result as $row) {
                  echo '
                  <li><a href="/blog/' . $row['id'] . '">' . $row['name'] . '</a></li>';
                  }
                  ?>
              </ul>
            </li>
            <?php 
              if (isset($_SESSION['id'])) {
                echo '<li ';  pageActive('user'); echo '><a href="/user">Mon compte</a></li>';
              } else {
                echo '<li '; pageActive('register'); echo '><a href="/register">S\'inscrire</a></li>';
              }
              ?>
             <li <?php pageActive('help'); ?>><a href="/help">Aide</a></li>
          </ul>
          <?php if (isset($_SESSION['id'])):?>
            <div class="navbar-form navbar-right">
              <form method="post" action="/controllers/UserController.php">
                <input name="logout" class="btn btn-primary" type="submit" valUe="Déconnexion">
              </form>
            </div>
          <?php else: ?>
            <form class="navbar-form navbar-right" role="form" method="post" action="/controllers/UserController.php">
              <div class="form-group">
                <input type="text" name="username" placeholder="Utilisateur" class="form-control">
              </div>
              <div class="form-group">
                <input type="password" name="password" placeholder="Mot de passe" class="form-control">
              </div>
              <button type="submit" name="login" class="btn btn-primary">Connexion</button>
            </form>

          <?php endif;?>
        </div>
        <!--/.navbar-collapse -->
      </div>
    </div>
    <?php
      if (isset($_GET['blog'])) {
      
        $blogid = stripslashes($_GET['blog']);	   
         if (isset($_GET['page']) && is_numeric($_GET['page'])) {
           $page = $_GET['page'];
         } else {
           $page = 1;
         }
      /* 
      $sql="SELECT * FROM $tbl_blog WHERE id = '$page'";
      $result = mysql_query($sql);
      	  if (mysql_num_rows($result) > 0) {
         while($row = mysql_fetch_array($result)) {
        echo $row['html'];
      }
       } else {
      include_once('pages/404.php'); */
      include_once('includes/blog.php');
      } elseif(isset($_GET['post'])) {
      $page = stripslashes($_GET['post']);	    
      include_once('includes/post.php');
      } elseif(isset($_GET['register'])) {
      include_once('includes/register.php');
      } elseif(isset($_GET['login'])) {
      include_once('includes/login.php');
      } elseif(isset($_GET['reset'])) {
      $resetcode = $_GET['reset'];
      include_once('includes/reset.php');
      } elseif(isset($_GET['search'])) {
      include_once('includes/search.php');
      } elseif(isset($_GET['help'])) {
      include_once('includes/help.php');
      } elseif(isset($_GET['user'])) {
        if (isset($_SESSION['id'])) {
          include_once('includes/user.php');
        } else {
          include_once('includes/forbidden.php');
        }
      } elseif(isset($_GET['newpost'])) {
        if (isset($_SESSION['id'])) {
          include_once('includes/new.php');
        } else {
          include_once('includes/forbidden.php');
        }
      } elseif(isset($_GET['activate'])) {
     	$code = (($_GET['activate'])? $_GET['activate'] : 1);	    
      include_once('includes/activate.php');
      } elseif(isset($_GET['raw'])) {

        if (isset($_SESSION['id'])) {
          if (fUser::isAdmin($_SESSION['id'] && !empty($_GET['raw']))) {
            $blogid = fBlog::getBlogByUser($_GET['raw']);
          } else {
            $blogid = fBlog::getBlogByUser($_SESSION['id']);
          }
          include_once('includes/raw.php');
        } else {
          include_once('includes/forbidden.php');
        }

      } elseif(isset($_GET['editpost'])) {
        if (isset($_SESSION['id'])) {
          $postid = $_GET['editpost'];
          include_once('includes/edit.php');
        } else {
          include_once('includes/forbidden.php');
        }
      } elseif(empty($_GET) || isset($_GET['home']) || isset($_GET['default'])) {
      include_once('includes/default.php');
      } else {
      include_once('includes/not_found.php');
      }
      ?>
    <!-- Bootstrap core JavaScript
      ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>
  </body>
</html>
<?php
  $buffer=ob_get_contents();
  ob_end_clean();
  $buffer=str_replace("%TITLE%",((isset($pagetitle))?$pagetitle:""), $buffer);
  echo $buffer;
?>