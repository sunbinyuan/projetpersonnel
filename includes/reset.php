<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">    
      <form action="/controllers/UserController.php" class="form-horizontal" method="post">
        <h2>Réinitialiser le mot de passe</h2>
        <?php
        if (isset($_SESSION['reset'])) {
          echo $_SESSION['reset'];
          unset($_SESSION['reset']);
        }
        if (empty($resetcode)) :
        ?>  
        <div class="control-group">
          <label class="control-label" for="email">Email</label>
          <div class="controls">
            <input class="form-control" name="email" placeholder="Email" type="text">
          </div>
        </div>
        <div class="control-group">
          <hr>
          <div class="controls">
            <button class="btn btn-default" type="submit" name="reset">Réinitialiser</button>
          </div>
        </div>
        <?php
        elseif(isset($resetcode)) :
          $user = fUser::where('resetcode', '=', $resetcode)[0];
          if(isset($user)) :
        ?>
        <div class="control-group">
          <label class="control-label" for="password">Mot de passe</label>
          <div class="controls">
            <input class="form-control" name="password" placeholder="Mot de passe" type="password">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="password">Confirmer le mot de passe</label>
          <div class="controls">
            <input class="form-control" name="confirmpassword" placeholder="Mot de passe" type="password">
          </div>
        </div>
        <input type="hidden" name="resetcode" value="<?php echo $resetcode; ?>">
        <div class="control-group">
          <hr>
          <div class="controls">
            <button class="btn btn-default" type="submit" name="resetpass">Réinitialiser</button>
          </div>
        </div>
        <?php
          else:
        ?>
        Il semblerait que vous avez cliqué sur un lien invalide. Veuillez essayer à nouveau.
        <?php
          endif;
        endif;
        ?>
      </form>
    </div>
  </div>
</div>
