<?php 
$pagetitle = "Mon compte - ";
if (isset($_GET['changepass'])) :
	include_once('includes/user/changepass.php');
elseif (isset($_GET['css'])) :
	include_once('includes/user/css.php');
elseif (isset($_GET['widget'])) :
	include_once('includes/user/widget.php');
elseif (isset($_GET['admin'])) :
	if (fUser::isAdmin($_SESSION['id'])) :
	  include_once('includes/user/admin.php');
	endif;
elseif (isset($_GET['adminuser'])) :
	if (fUser::isAdmin($_SESSION['id'])) :
		include_once('includes/user/adminuser.php');
	endif;
else :
	$blogExists = fBlog::check($_SESSION['id']);
?>
<div class="container">
	<!-- Example row of columns -->
	<div class="row">
		<div class="col-md-12">
			<h2>Mon compte</h2>

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
			  <li class="active"><a href="#blog" role="tab" data-toggle="tab">Blog</a></li>
        <li><a href="#profile" role="tab" data-toggle="tab">Compte</a></li>
        <?php if (fUser::isAdmin($_SESSION['id'])) : ?>
        <!-- If user is admin -->
			  <li><a href="#admin" role="tab" data-toggle="tab">Admin</a></li>
				<?php endif; ?>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane active" id="blog">
					<ul class="nav nav-pills nav-stacked">
						<li><a href="#" data-toggle="modal" data-target="#blogCreate"><span class="glyphicon glyphicon-plus"></span> <?php echo (($blogExists)?'Changer le nom de mon blog':'Cr&eacute;er mon blog'); ?></a></li>
						<li><a href="/newpost"><span class="glyphicon glyphicon-plus-sign"></span> Ajouter une entr&eacute;e</a></li>
						<li><a href="/user-css"><span class="glyphicon glyphicon-pencil"></span> Changer le style (CSS)</a></li>
						<li><a href="/user-widget"><span class="glyphicon glyphicon-star"></span> Gérer les widgets</a></li>
						<li><a href="/raw"><span class="glyphicon glyphicon-book"></span> Voir mon blog en raw</a></li>
						<li><a href="#" data-toggle="modal" data-target="#blogDelete"><span class="glyphicon glyphicon-remove"></span> Supprimer mon blog</a></li>
					</ul>
			  </div>
			  <div class="tab-pane" id="profile">
					<ul class="nav nav-pills nav-stacked">
						<li><a href="/user-changepass"><span class="glyphicon glyphicon-asterisk"></span> Changer mon mot de passe</a></li>
					</ul>
			  </div>
        <?php if (fUser::isAdmin($_SESSION['id'])) : ?>
        <!-- If user is admin -->
        <div class="tab-pane" id="admin">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="/user-admin"><span class="glyphicon glyphicon-user"></span> Administrer les utilisateurs</a></li>
          </ul>
        </div>
        <?php endif; ?>   
			</div>
		</div>
	</div>
</div> <!-- /container -->
</div>
<?php
	if (blogExists) {
		include_once('includes/user/changeBlogModal.php');
	} else {
		include_once('includes/user/createBlogModal.php');
	}
?>


<div class="modal fade" id="blogDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-sm">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
			</button>
			<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
		</div>
		<div class="modal-body">
			<p>Êtes-vous sûr de vouloir supprimer votre blog?</p>
			<div class="form-group pull-right">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
				<a href="/controllers/BlogController.php?deleteblog" class="btn btn-primary">Oui</a>
			</div>
		</div>
		<style type="text/css">
		.modal-footer{
		border-top:0px;
		}
		</style>
		<div class="modal-footer">
		</div>
	</div>
</div>
<?php 
endif;
?>