<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">
      <?php 
      if (isset($_SESSION['id'])) {
        if (fBlog::getBlogByUser($_SESSION['id']) == fPost::getBlogByPost($postid) || fUser::isAdmin($_SESSION['id'])) {
          $row = fPost::where('id', '=', $postid)[0];
        ?>
      <h2>Modifier une entr&eacute;e</h2>
      <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
      <form method="post" action="/controllers/PostController.php">
        <input type="hidden" name="id" value="<?php echo $postid; ?>">
        <input type="hidden" name="publishdate" value="<?php echo $row['publishdate']; ?>">      
        <div class="form-group">
          <input type="text" name="title" id="title" placeholder="Titre" class="form-control" value="<?php echo $row['title']; ?>">
        </div>
        <span class="help-block">
          <a href="http://getbootstrap.com/css/" target="_blank">Pour plus d\'instruction, cliquez ici</a>
        </span>
        <textarea id="editor1" name="content" class="form-control" style=style="width: 100%; height: 200px;">
          <?php echo $row['content']; ?>
        </textarea>
        <script type="text/javascript">
          CKEDITOR.env.isCompatible = true;
          CKEDITOR.replace( 'editor1' );
        </script>
        </p>
        <p>
          <button type="submit" class="btn btn-default" name="edit"><i class="glyphicon glyphicon-ok"></i> Submit</button>      
        </p>   
      </form>
      <?php
        } else {
          echo '
            <h2>Ajouter une entr&eacute;e</h2>
            <div class="alert alert-danger">
                Vous ne possedez pas de blog!
            </div>';
        }
      } else {

      }
      ?>
    </div>
  </div>
</div>