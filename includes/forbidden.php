<?php 
  $pagetitle = "403 Accès interdit - ";
  http_response_code(403);  
?>
<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">
      <h2>403 - Acc&egrave;s interdit</h2>    
      <div class="alert alert-danger">Veuillez vous connecter pour performer cette action.</div>
    </div>
  </div>
</div>