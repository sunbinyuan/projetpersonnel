<div class="container">
  <!-- Example row of columns -->  
  <div class="row">
    <div class="col-md-12">		
    	<h2>Choisir le style</h2>
	    <?php
				$pagetitle = "Modifier le CSS - ";
				if (isset($_SESSION['id'])):
					$userid = $_SESSION['id'];
					if (isset($_GET['id'])):
						$result = fCSS::getCSS('id', '=', $_GET['id'], 'AND', 'userid', '=', $userid)[0];
						if (isset($result)):
							$row = $result;
							?>
			<form method="post" action="/controllers/CSSController.php">
        <div class="form-group">
          <input type="text" name="title" id="title" placeholder="Titre" class="form-control" value="<?php echo $row['name']; ?>">
        </div>
        <span class="help-block">
        	<a href="http://www.w3schools.com/css/default.asp" target="_blank">Pour plus d\'instruction, cliquez ici</a>
        </span>
        <textarea id="editor1" name="content" style="width: 100%; height: 200px;" class="form-control"><?php echo $row['css']; ?>
			</textarea>
        </p>
        <input type="hidden" name="css" value="<?php echo $_GET['id']; ?>"
        <p>
          <button type="submit" name="modify" class="btn btn-default"><i class="glyphicon glyphicon-ok"></i> Submit</button>		
        </p>
      </form>
				<?php else: ?>
		        <div class="alert alert-danger">Ce style n'existe pas ou ne vous appartient pas.</div>';
				<?php	endif;
				else:
					$result = fCSS::getCSS('userid', '=', $userid);
					echo '
					<form method="post" action="/controllers/CSSController.php">
          <table class="table table-condensed" align="center">
            <thead>
              <tr>
                <th width="16px">Activé</th>
                <th>Nom</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th><input type="radio" name="css" id="css" value="default" checked></th>
                <td>Défaut</td>
              </tr>';
          if(!empty($result)) {
            foreach($result as $row) {
              echo '
              <tr>
                <th><input type="radio" name="css" id="css" value="' . $row['id'] .'"' . (($row['selected'] == 1)?" checked":"") .'></th>
                <td><a href="' . basename($_SERVER['REQUEST_URI']) . '-id/' . $row['id'] . '">' . $row['name'] . '</a></td>
              </tr>';
            }
          }
          echo '
            </tbody>
          </table>
					<button name="add" class="btn btn-default btn-sm" type="submit">Ajouter</button>
					<button name="delete" class="btn btn-default btn-sm" type="submit">Supprimer</button>
					<button name="select" class="btn btn-default btn-sm" type="submit">Selectionner</button>
				</form>';
				endif;
			else:
        echo '
        <div class="alert alert-danger">Veuillez vous connecter pour performer cette action.</div>';
			endif;
			?>    
	</div>
  </div>
</div>