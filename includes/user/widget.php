<?php
  $pagetitle = "Gérer les widgets - ";
?>
<div class="container">
  <!-- Example row of columns -->  
  <div class="row">
    <div class="col-md-12">   
      <h2>Gérer les widgets</h2>
      <?php
        $userid = $_SESSION['id'];
        if (isset($_GET['id'])):
          $result = fWidget::widgetFetch('id', '=', $_GET['id'], 'AND', 'userid', '=', $userid)[0];
          if (isset($result)):
            $row = $result;
            ?>
      <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
      <form method="post" action="/controllers/WidgetController.php">
        <div class="form-group">
          <input type="Nom" name="name" id="name" placeholder="Nom" class="form-control" value="<?php echo $row['name']; ?>">
        </div>
        <span class="help-block">
          <a href="http://getbootstrap.com/css/" target="_blank">Pour plus d'instruction, cliquez ici</a>
        </span>
        <textarea id="editor1" name="content" style="width: 100%; height: 200px;"><?php echo $row['content']; ?></textarea>
        <script type="text/javascript">
          CKEDITOR.env.isCompatible = true;
          CKEDITOR.replace( 'editor1' );
        </script>
        </p>
        <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"
        <p>
          <button type="submit" name="modify" class="btn btn-default"><i class="glyphicon glyphicon-ok"></i> Submit</button>    
        </p>
      </form>
        <?php else: ?>
            <div class="alert alert-danger">Ce widget n'existe pas ou ne vous appartient pas.</div>';
        <?php endif;
        else:
          $result = fWidget::widgetFetch('userid', '=', $userid);
          echo '
          <form method="post" action="/controllers/WidgetController.php">
          <table class="table table-condensed" align="center">
            <thead>
              <tr>
                <th width="16px">Activé</th>
                <th>Nom</th>
                <th style="text-align: right;">Ordre d\'affichage</th>
              </tr>
            </thead>
            <tbody>';
            if(!empty($result)) {
              foreach($result as $row) {
                echo '
              <tr>
                <th scope="row">
                  <input type="hidden" name="enabled[' . $row['id'] .']]" value="0">
                  <input type="checkbox" name="enabled[' . $row['id'] .']]" id="css" value="1"' . (($row['enabled'] == 1)?" checked":null) .'>
                </th>
                <td><a href="' . basename($_SERVER['REQUEST_URI']) . '-id/' . $row['id'] . '">' . $row['name'] . '</a></th>
                <td align="right" class="col-xs-3">
                  <input type="text" name="displayorder[' . $row['id'] . ']]" class="form-control" value="' . $row['displayorder'] . '">
                </th>
              </tr>';
              }
            }
            echo '
            </tbody>
          </table>

          <button name="add" class="btn btn-default btn-sm" type="submit">Ajouter</button>
          <button name="delete" class="btn btn-default btn-sm" type="submit">Supprimer</button>
          <button name="save" class="btn btn-default btn-sm" type="submit">Sauvegarder</button>
        </form>';
        endif;
      ?>    
  </div>
  </div>
</div>