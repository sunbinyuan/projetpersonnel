<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">
      <h2>Administration</h2>
      <table class="table table-condensed">
      <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Mot de passe</th>
          <th>Email</th>
          <th>Activé</th>
          <th>Dernière connexion</th>
          <th>IP</th>
          <th>Admin</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $users = fUser::all();
        //var_dump($users);
        foreach ($users as $user) :
          if (!empty($user['lastlogin'])) {
            $lastlogin = date('d/m/Y H:i:s', $user['lastlogin']);
          } else {
            $lastlogin = 'Jamais';
          }
          echo '
        <tr>
          <th scope="row">' . $user['id'] . '</th>
          <td>'. $user['username'] .'</td>
          <td>-CONFIDENTIEL-</td>
          <td>' . $user['email'] . '</td>
          <td>' . (($user['active'] == 1)?'Oui':'Non') . '</td>
          <td>' . $lastlogin . '</td>
          <td>' . $user['ip'] . '</td>
          <td>' . (($user['admin'] == 1)?'Oui':'Non') . '</td>
          <td>
            <a href="/user-adminuser-id/' . $user['id'] . '" title="Modifier les infos de l\'utilisateur"><span class="glyphicon glyphicon-wrench"></span></a>
            <a href="#" onclick="blogDelete(' . $user['id'] . ')" title="Supprimer son blog"><span class="glyphicon glyphicon-remove"></span></a>
            <a href="/raw/' . $user['id'] . '" title="Voir le blog en raw"><span class="glyphicon glyphicon-book"></span></a>
          </td>
        </tr>
        ';
        endforeach; ?>
      </tbody>
    </table>
    </div>
  </div>
</div>

<div class="modal fade" id="blogDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        &times;
        </button>
        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
      </div>
      <div class="modal-body">
        <p>Êtes-vous sûr de vouloir supprimer votre blog?</p>
        <div class="form-group pull-right">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
          <a href="" class="btn btn-primary admin blogDelete">Oui</a>
        </div>
      </div>
      <style type="text/css">
      .modal-footer{
      border-top:0px;
      }
      </style>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>