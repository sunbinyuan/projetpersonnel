<?php
if (isset($_GET['id'])) :
  $userid = $_GET['id'];
  $user = fUser::where('id', '=', $userid)[0];
  if(!empty($user)) :
?>
<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">
    <h2>Modifier l'utilisateur</h2>
      <form action="/controllers/AdminController.php" class="form-horizontal" method="post">
        <input name="id" type="hidden" value="<?php echo $userid; ?>">
        <div class="control-group">
          <label class="control-label" for="username">Utilisateur</label>
          <div class="controls">
            <input class="form-control" name="username" placeholder="Utilisateur" type="text" value="<?php echo $user['username']; ?>">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="password">Mot de passe (laissez blanc si aucun changement)</label>
          <div class="controls">
            <input class="form-control" name="password" placeholder="Mot de passe" type="password" value="">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="password">Email</label>
          <div class="controls">
            <input class="form-control" name="email" placeholder="Ex: user@edu.csdm.qc.ca" type="email" value="<?php echo $user['email']; ?>">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="password">Activé</label>
          <div class="controls">
            <select class="form-control" name="active">
              <option value="1" <?php echo (($user['active'] == 1)?'selected':null); ?>>Oui</option>
              <option value="0" <?php echo (($user['active'] == 0)?'selected':null); ?>>Non</option>
            </select>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="password">Administrateur</label>
          <div class="controls">
            <select class="form-control" name="admin">
              <option value="1" <?php echo (($user['admin'] == 1)?'selected':null); ?>>Oui</option>
              <option value="0" <?php echo (($user['admin'] == 0)?'selected':null); ?>>Non</option>
            </select>
          </div>
        </div>
        <div class="control-group">
          <hr>
          <div class="controls">
            <button class="btn btn-primary" type="submit" name="usermodify">Modifier</button>
            <button class="btn btn-danger" type="submit" name="userdelete">Supprimer</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
    else:
?>
<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">
      <h2>Modifier l'utilisateur</h2>    
      <div class="alert alert-danger">Désolé, cet utilisateur n'existe pas.</div>
    </div>
  </div>
</div>
<?php
  endif;
else:
?>
<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">
      <h2>Modifier l'utilisateur</h2>    
      <div class="alert alert-danger">Désolé, cet utilisateur n'existe pas.</div>
    </div>
  </div>
</div>
<?php
endif;
?>