<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">    
      <form action="/controllers/UserController.php" class="form-horizontal" method="post">
        <h2>Modifier le mot de passe</h2>
        <?php
        if (isset($_SESSION['changepass'])) {
          echo $_SESSION['changepass'];
          unset($_SESSION['changepass']);
        }
        ?>
        <div class="control-group">
          <label class="control-label" for="password">Mot de passe</label>
          <div class="controls">
            <input class="form-control" name="password" placeholder="Mot de passe" type="password">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="password">Confirmer le mot de passe</label>
          <div class="controls">
            <input class="form-control" name="confirmpassword" placeholder="Mot de passe" type="password">
          </div>
        </div>
        <div class="control-group">
          <hr>
          <div class="controls">
            <button class="btn btn-default" type="submit" name="changepass">Modifier</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
