<div class="modal fade" id="blogCreate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form method="post" action="/controllers/BlogController.php">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Cr&eacute;er un blog</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon">
            <i class="glyphicon glyphicon-user"></i>
            </span>
            <input class="form-control" type="text" placeholder="Nom, Prénom" name="name">
          </div>
        </div>
        <div class="form-group pull-right">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
          <button type="submit" name="blogCreate" class="btn btn-primary">Cr&eacute;er</button>
        </div>
      </div>
      <style type="text/css">
      .modal-footer{
      border-top:0px;
      }
      </style>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</form>
</div>