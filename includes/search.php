<?php 
  if(isset($_GET['q'])) {
    $q = $_GET['q'];
  }
  ?>
<style type="text/css">
  body {
  padding-top: 75px;
  }
</style>
<script>
    window.onload = function() {
        document.forms[0].q.focus();
        var que = window.location.search.split("?")[1].split("&");
        for (var i = 0; i < que.length; i++) {
            var qa = que[i].split("=");
            if (qa[0] == 'q') {
                document.getElementById('q').value = decodeURIComponent(qa[1]).replace(/\+/g, ' ');
                break
            }
        }
    }
</script>
<script>
  (function() {
    var cx = '015566656005030166160:8ldolkvevha';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <form action="/search" method="GET">
        <div class="input-group">
          <input type="text" class="form-control" id="q" name="q" value="<?php echo $_GET['q']; ?>">
          <span class="input-group-btn">
          <button type="submit" class="btn btn-default" type="button">
          <span class="glyphicon glyphicon-search"></span>
          </button>
          </span>
        </div>
      </form>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <gcse:searchresults-only></gcse:searchresults-only>
    </div>
  </div>
</div>