<?php

$row = fBlog::where('id', '=', $blogid)[0];

$userid = $row['userid'];

$limit = ($page - 1) * 8;

?>
<link href="/css/blog_style.css" rel="stylesheet">
<?php
$css = fCSS::getCSS('userid', '=', $userid, 'AND', 'selected', '=', 1)[0];
if (!empty($css)) {
  echo '
  <style type="text/css">
  ' . $css['css'] . '
  </style>';
}
?>
<div class="container">
  <div class="row">
    <?php
    if (isset($row)) {
    $pagetitle = "{$row['name']} - ";
    $numpage = ceil(fPost::count($blogid) / 8);
    ?>
    <div class="col-lg-8">
      <?php
      $result = fPost::getPosts(
        array(
          'blogid' => $blogid,
          'index' => $limit,
          'limit' => 8,
          ));
      if (!empty($result)) {
        foreach ($result as $row) {
        echo '
          <div class="post">
              <h1><a href="/post/' . $row['id'] . '">' . $row['title'] . '</a>
              </h1>
              <hr>
              <p>
                <span class="glyphicon glyphicon-time"></span> ' . date("Y-m-d H:i:s", $row['publishdate']) . '</p>
              <hr>
              <p>' . $row['content'] . '</p>';
          if (isset($_SESSION['id'])) {
            if ($_SESSION['id'] == $userid || fUser::isAdmin($_SESSION['id'])) {
              echo '
              <br>
              <form method="POST" action="/controllers/PostController.php">
                <input type="hidden" name="id" value="' . $row['id'] . '">
                <a class="btn btn-default btn-sm" href="/editpost/' . $row['id'] . '">Modifier</span></a> 
                <input type="submit" class="btn btn-danger btn-sm" name="delete" value="Supprimer">
              </form>';
            }
          }
          echo '
          </div>';
        
        }
      } else {
        echo '
        <div class="no-post">
          <div class="alert alert-info">Il n\'y a aucune entr&eacute;e &agrave; montrer.</div>
        </div>';
      }
      ?>
      <ul class="pagination">
        <?php
        echo '<li' . (($page <= 1)?' class="disabled"':'') . '><a href="/blog/' . $blogid .'/page/' . ($page - 1) . '">&laquo;</a></li>';
        
        for ($i = 1; $i <= $numpage; $i++) {
        echo '<li' . (($i == $page)?' class="active"':'') . '><a href="/blog/' . $blogid . '/page/' . $i . '">'. $i .'</a></li>';
        }
        echo '<li' . (($page >= $numpage)?' class="disabled"':'') . '><a href="/blog/' . $blogid .'/page/' . ($page + 1) . '">&raquo;</a></li>';
        ?>
      </ul>
    </div>
    <div class="col-lg-4">
      <div class="well">
        <h4>Blog Search</h4>
        <form action="/script/actions.php?search" method="POST">
          <div class="input-group">
            <input type="text" class="form-control" name="q">
            <span class="input-group-btn">
            <button type="submit" class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-search"></span>
            </button>
            </span>
          </div>
        </form>
      </div>
      <div class="well">
        <h4>Entr&eacute;es r&eacute;centes</h4>
        <div class="row">
          <div class="col-lg-12">
            <ul class="list-unstyled">
              <?php
              $result = fPost::getPosts(
                array(
                  'blogid' => $blogid,
                  'index' => 0,
                  'limit' => 5,
                )
              );

              if (isset($result)) {
                foreach ($result as $row) {
                  echo '
                    <li><a href="/post/' . $row['id'] . '">' . $row['title'] . '</a>
                    </li>';
                }
              } else {
                echo 'Il n\'y a aucune entr&eacute;e &agrave; montrer.';
              } ?>
            </ul>
          </div>
        </div>
      </div>
      <?php
        $widgets = fWidget::widgetFetch('userid', '=', $userid, 'AND', 'enabled', '=', 1, 'ORDER BY displayorder');
        if (!empty($widgets)) {
          foreach ($widgets as $widget) {
            echo '
      <div class="well"> 
      ' . $widget['content'] . '
      </div>';
          }
        }
      ?>
    </div>

  <?php } else {
  http_response_code(404);
  echo
    '<div class="col-lg-12">
      <div class="alert alert-danger">D&eacute;sol&eacute;, ce blog est inexistant.</div>
    </div>';
  }
  ?>
  </div>
</div>