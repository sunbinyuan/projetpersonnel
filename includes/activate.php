<?php
  $user = fUser::where('code', '=', $code)[0];
  if(isset($user)) {
    if ($user['active'] == 0) {
      if(fUser::userActivate($user['id'])) {
        $_SESSION['global'] = "Vous êtes activé(e)! Vous pouvez maintenant vous connecter sur le site.";
      } else {
        $_SESSION['global'] = "Une erreur s'est produite lors de l'activation. Veuillez réessayer plus tard.";
      }
    } else {
      $_SESSION['global'] = "Votre compte est déjà activé.";
    }
  } else {
    $_SESSION['global'] = "Ce code n\'est lié à aucun compte.";
  }
  header("Location: /");
?>