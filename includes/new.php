<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">
      <?php 
      if (isset($_SESSION['id'])) {
        $result = fBlog::check($_SESSION['id']);
        if ($result) {
        ?>
      <h2>Ajouter une entr&eacute;e</h2>
      <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
      <form method="post" action="/controllers/PostController.php">
        <div class="form-group">
          <input type="text" name="title" placeholder="Titre" class="form-control">
        </div>
        <span class="help-block">
          <a href="http://getbootstrap.com/css/" target="_blank">Pour plus d\'instruction, cliquez ici</a>
        </span>
        <textarea id="editor1" name="content" class="form-control" style=style="width: 100%; height: 200px;"></textarea>
        <script type="text/javascript">
          CKEDITOR.env.isCompatible = true;
          CKEDITOR.replace( 'editor1' );
        </script>
        </p>
        <p>
          <button type="submit" class="btn btn-default" name="post"><i class="glyphicon glyphicon-ok"></i> Submit</button>		
        </p>
      </form>
      <?php
        } else {
          echo '
          	<h2>Ajouter une entr&eacute;e</h2>
            <div class="alert alert-danger">
        		Vous n\'avez pas la permission pour modifer cet entr&eacute;e.
        	</div>';
        }
      } else {
          echo '
          <h2>Modifier une entr&eacute;e</h2>
          <div class="alert alert-danger">Veuillez vous connecter pour performer cette action.</div>';
      }
      ?>
    </div>
  </div>
</div>