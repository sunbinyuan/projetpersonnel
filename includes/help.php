<?php	$pagetitle = "Aide - ";?>
<div class="container">
	<!-- Example row of columns -->
	<div class="row">
		<div class="col-md-12">
			<h2>Aide</h2>
			<hr>
			<dl>
				<dt>Qu'est ce que ce site?</dt>
				<dd>Ce site est le projet personnel de Binyuan Sun et sert à regrouper et héberger les journaux de bords des élèves en forme de blog.</dd>
				<dt>Comment puis-je avoir le mien?</dt>
				<dd>Inscrivez vous à l'aide de votre email de la CSDM en suivant <a href="/register">ce lien</a>. Une fois votre compte activé, allez dans «<a href="/user">Mon compte</a>» puis «Créer mon blog».</dd>
				<dt>Pourquoi un ai-je besoin d'un compte de la CSDM?</dt>
				<dd>Ce site est présentement seulement utilisé par les élèves de l'École Internationale de Montréal. Si vous désirez l'utiliser pour vous, envoyez moi un email à mon courriel.</dd>
				<dt>J'ai perdu mon mot de passe</dt>
				<dd>Suivez <a href="/reset">ce lien</a> puis entrez votre courriel utilisé durant l'inscription. Un lien pour réinitialiser votre mot de passe y sera envoyé.</dd>
				<dt>Comment ce site a été fait?</dt>
				<dd>Ce site utilise le langage serveur PHP et utilise la base de données MySQL. Bootstrap a été utilisé pour sauver du temps au niveau du CSS. D'autres extensions telles que CKEditor a été utiliser pour le traitement du text et Disqus pour le système de commentaires</dd>
			</dl>
			<p>N'hésitez pas à me rejoindre concernant les questions ou les bugs. Les commentaires et les suggestions sont toujours appréciés!</p>
			<p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <a href="mailto:sunbinyuan01@hotmail.com">sunbinyuan01@hotmail.com</a></p>
		</div>
	</div>
</div> <!-- /container -->
