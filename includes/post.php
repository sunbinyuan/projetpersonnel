<?php 

//Disqus SSO
Disqus::SSO(array(
            "id" => $_SESSION['id'],
            "username" => $_SESSION['username'],
            "email" => $_SESSION['email']
        )
);
?>
<link href="/css/blog_style.css" rel="stylesheet">
<?php
$blogid = fPost::getBlogByPost($page);
$userid = fBlog::getUserByBlog($blogid);
$css = fCSS::getCSS('userid', '=', $userid, 'AND', 'selected', '=', 1)[0];

if (!empty($css)) {
  echo '
  <style type="text/css">
  ' . $css['css'] . '
  </style>';

}
?>
<div class="container">
  <div class="row">
    <?php
      $row = fPost::where('id', '=', $page)[0];
      if (isset($row)) {
    ?>
    <div class="col-lg-8">
      <?php
        $pagetitle = $row['title'] . " - ";
      	echo '
      		<div class="post">
      			<h1><a href="/post/' . $row['id'] . '">' . $row['title'] . '</a>
      			</h1>
      			<hr>
      			<p>
      				<span class="glyphicon glyphicon-time"></span> ' . date("Y-m-d H:i:s", $row['publishdate']) . '</p>
      			<hr>
      			<p>' . $row['content'] . '</p>';
      	if (isset($_SESSION['id'])) {
      		if ($_SESSION['id'] == $userid || fUser::isAdmin($_SESSION['id'])) {
      			echo '<br><a class="btn btn-default btn-sm" href="/?editpost=' . $row['id'] . '">Modifier</span></a> <a class="btn btn-danger btn-sm" href="/script/actions.php?deletepost=' . $row['id'] . '">Supprimer</span></a>';
      		}
      	}
        echo "</div>"
      ?>
      <hr>
      <div id="disqus_thread"></div>
      <script type="text/javascript">
          /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
          var disqus_shortname = 'projetpersonnel'; // required: replace example with your forum shortname

          /* * * DON'T EDIT BELOW THIS LINE * * */
          (function() {
              var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
              dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
          })();
      </script>
      <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
      <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>    
    </div>
    <div class="col-lg-4">
      <div class="well">
        <h4>Blog Search</h4>
        <form action="/script/actions.php?search" method="POST">
          <div class="input-group">
            <input type="text" class="form-control" name="q">
            <span class="input-group-btn">
            <button type="submit" class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-search"></span>
            </button>
            </span>
          </div>
        </form>
      </div>
      <div class="well">
        <h4>Entr&eacute;es r&eacute;centes</h4>
        <div class="row">
          <div class="col-lg-12">
            <ul class="list-unstyled">
              <?php
              $result = fPost::getPosts(
                array(
                  'blogid' => $blogid,
                  'index' => 0,
                  'limit' => 5,
                )
              );

              if (isset($result)) {
                foreach ($result as $row) {
                  echo '
                    <li><a href="/post/' . $row['id'] . '">' . $row['title'] . '</a>
                    </li>';
                }
              } else {
                echo 'Il n\'y a aucune entr&eacute;e &agrave; montrer.';
              } ?>
            </ul>
          </div>
        </div>
      </div>
      <?php
        $widgets = fWidget::widgetFetch('userid', '=', $userid, 'AND', 'enabled', '=', 1, 'ORDER BY displayorder');
        if (!empty($widgets)) {
          foreach ($widgets as $widget) {
            echo '
      <div class="well"> 
      ' . $widget['content'] . '
      </div>';
          }
        }
      ?>
    </div>
  <?php } else {
    http_response_code(404);
    echo 
    '<div class="col-lg-12">
    	<div class="alert alert-danger">D&eacute;sol&eacute;r, cette entr&eacute;e est inexistant.</div>
    </div>';
    } 
    ?>
  </div>
</div>