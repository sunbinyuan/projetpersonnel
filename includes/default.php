<style>
  #intro {
    background-image: url('/images/background.png');
    color: #fff;
    font-weight: 300;
    text-align: center;
    min-height: 100%;
    padding: 50px 0;
  }
  .intro-footer {
    margin-top: 100px;
  }
</style>

<section id="intro">
  <div class="container">
    <div class="row">
      <div>
        <div class="col-md-12">
          <?php
          if (isset($_SESSION['global'])) {
            echo '<div class="alert alert-info" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Info:</span>
                    ' . $_SESSION['global'] . '
                  </div>';
            unset($_SESSION['global']);
          }
          ?>
          <h2>Hébergez votre journal de bord en ligne gratuitement</h2>
          <h3>Mon JournaldeBord est destiné pour tous ceux et celles <br/>qui désirent avoir un blog comme journal de bord.</h3>
          <div class="box">
            <a href="/register" class="btn btn-lg btn-primary">Inscription</a>
          </div>
        </div>
      </div>
    </div>
    <div class="row intro-footer">
      <div class="col-md-4 text-center">
        <div class="large-icon">
          <span class="glyphicon glyphicon-gift" aria-hidden="true"></span>
        </div>
        <h3>Complètement gratuit</h3>
        <p>Héberger son journal de bord sur ce site ne vous coutera absolument rien</p>
      </div>
      <div class="col-md-4 text-center">
        <div class="large-icon">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
        </div>
        <h3>Facile à utiliser</h3>
        <p>Simplement copier/coller de votre document Word ou tout autre éditeur de texte sans avoir a écrire une seule ligne de code</p>
      </div>
      <div class="col-md-4 text-center">
        <div class="large-icon">
          <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </div>
        <h3>Personnalisable</h3>
        <p>Modifier le CSS et ajouter des widgets pour rendre votre blog unique</p>
      </div>


    </div>
  </div>
</section>
<!-- /container -->	