<?php
  $pagetitle = "404 Page non trouv&eacute;e - ";
  http_response_code(404);
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h2>404 - Page non trouv&eacute;e</h2>
			<div class="alert alert-danger">D&eacute;sol&eacute;, la page que vous d&eacute;sirez est inexistante.</div>
    </div>
  </div>
</div>

  