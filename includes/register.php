<div class="register">
  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <div class="col-md-12">
        <form action="/controllers/UserController.php" class="form-horizontal" method="post">
          <h2>S'inscrire</h2>
          <div class="control-group">
            <label class="control-label" for="username">Utilisateur</label>
            <div class="controls">
              <input class="form-control" name="username" placeholder="Utilisateur" type="text">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="password">Mot de passe</label>
            <div class="controls">
              <input class="form-control" name="password" placeholder="Mot de passe" type="password">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="password">Confirmer le mot de passe</label>
            <div class="controls">
              <input class="form-control" name="confirmpassword" placeholder="Mot de passe" type="password">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="password">Email de la CSDM</label>
            <div class="controls">
              <input class="form-control" name="email" placeholder="Ex: user@edu.csdm.qc.ca" type="email">
              <small>*Remarque: L'adresse courriel des élèves de la CSDM est sous ce format: «utilisateur@edu.csdm.qc.ca»</small>
            </div>
          </div>
          <div class="control-group">
            <hr>
            <div class="controls">
              <button class="btn btn-default" type="submit" name="register">S'inscrire</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>