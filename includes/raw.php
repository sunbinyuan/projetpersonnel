<?php 
  $pagetitle = "Mon journal de bord - ";
?>
<link href="/css/blog_style.css" rel="stylesheet">
<style>
  .post {
    font-family: "Times New Roman";
    font-size: 12pt;
    line-height: 1.5;
  }
  div.title>h1 {
    font-size: 14pt;
    font-weight: bold;
    text-decoration: underline;
  }
  p.time {
    font-style: italic;
  }
</style>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
    <?php
      if (isset($blogid)) {
        $result = fPost::getPosts(
          array(
            'blogid' => $blogid,
            ));
        if (!empty($result)) {
          foreach ($result as $row) {
          echo '
            <div class="post">
              <div class="title"><h1>' . $row['title'] . '</h1></div>
              <p class="time">' .
                 date("Y-m-d H:i:s", $row['publishdate']) . '</p>
              <div class="content">' . $row['content'] . '</div>
            </div>'; 
          }
        } else {
          echo '
            Il n\'y a aucune entr&eacute;e &agrave; montrer.';
        }
      } else {
        echo
          'Vous ne poss&eacute;dez pas de blog.';
      }
      echo '</div>';
  ?>
  </div>
</div>