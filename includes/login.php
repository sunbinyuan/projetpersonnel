<div class="container">
  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-12">    
      <form action="/controllers/UserController.php" class="form-horizontal" method="post">
        <h2>Se connecter</h2>
        <?php
        if (isset($_SESSION['login'])) {
          echo $_SESSION['login'];
          unset($_SESSION['login']);
        }
        ?>  
        <div class="control-group">
          <label class="control-label" for="username">Utilisateur</label>
          <div class="controls">
            <input class="form-control" name="username" placeholder="Utilisateur" type="text">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="password">Mot de passe</label>
          <div class="controls">
            <input class="form-control" name="password" placeholder="Mot de passe" type="password">
            <p class="text-muted"><a href="/reset">Mot de passe oublié ?</a></p>
          </div>
        </div>
        <div class="control-group">
          <hr>
          <div class="controls">
            <button class="btn btn-default" type="submit" name="login">S'inscrire</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
