<?php
require_once dirname(__FILE__) . "/../models/models.php";
header('Content-Type: text/html; charset=utf-8');

if (isset($_POST['blogCreate'])) {
  session_start();
  $bool = fBlog::check($_SESSION['id']);
  if ($bool == false) {
    if(fBlog::add(
      array(
        'name' => $_POST['name'],
        'userid' => $_SESSION['id'],
      )
    ))
    {
      header("Location: /user");
    } else {
      echo "Une erreur s'est produite lors de la création du blog. Veuillez réssayer plus tard.";
    }
  } else {
  echo 'Vous avez déjà un blog.';
  }
} elseif (isset($_POST['blogChange'])) {
  session_start();
  if(
    fBlog::blogChange(
      array(
        'name' => $_POST['name'],
        'userid' => $_SESSION['id'],
      )
    )
  )
  {
    header("Location: /user");
  } else {
    echo "Une erreur s'est produite lors de la création du blog. Veuillez réssayer plus tard.";
  }
} elseif (isset($_GET['deleteblog'])) {
  session_start();
  if(fBlog::remove($_SESSION['id']))
  {
    header("Location: /user");
  } else {
    echo "Une erreur s'est produite lors de la création du blog. Veuillez réssayer plus tard.";
  }
} else {
	header("Location :/");
}