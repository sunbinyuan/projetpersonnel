<?php
require_once dirname(__FILE__) . "/../models/models.php";
header('Content-Type: text/html; charset=utf-8');


session_start();

if (isset($_POST['add'])) {
	if (fCSS::count($_SESSION['id']) >= 5) {
		echo "Vous avez dépassé le nombre maximal de styles.";
	} else {
		try {
			if (fCSS::add($_SESSION['id'])) {
				header("Location: /user-css-id/" . fCSS::connect()->lastInsertId());
			}
		} catch(Exception $e) {
			echo $e->getMessage();
		}
	}
} elseif (isset($_POST['delete'])) {
	fCSS::remove(
		array(
		'id' => $_POST['css'],
		'userid' => $_SESSION['id']
		)
	);
  header("Location: /user-css");
} elseif (isset($_POST['select'])) {
	fCSS::select(
		array(
		'id' => $_POST['css'],
		'userid' => $_SESSION['id']
		)
	);
	header("Location: /user-css");
} elseif (isset($_POST['modify'])) {
	  $name = strip_tags($_POST['title']);
	  $css = strip_tags($_POST['content']);
	  try {
		  if (fCSS::modify(
		  	array(
		  	'id' => $_POST['css'],
		  	'userid' => $_SESSION['id'],
		  	'name' => $name,
		  	'css' => $css,
		  	)
		  )) 
		  {
		  	header("Location: /user-css");
		  }
		} catch(Exception $e) {
			echo $e->getMessage();
		}
} else {
	header("Location :/");
}