<?php
require_once dirname(__FILE__) . "/../models/models.php";
header('Content-Type: text/html; charset=utf-8');

function quickRandom($length = 16)
{
  $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

  return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
} 

if (isset($_POST['login'])) {
	$username = $_POST['username']; 
	$password = $_POST['password']; 
	if (fUser::userLogin(array(
	'username' => $username,
	'password' => md5($password)
		)
	))
	{
    $referer = $_SERVER['HTTP_REFERER'];
    $sitedomain = $_SERVER['HTTP_HOST'];
    if (parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) !== $sitedomain || strpos($_SERVER['HTTP_REFERER'], "login") !== false) {
		 header("Location: /");
    } else {
      header("Location: $referer");
    }
	} else {
    header("Location: /login");
  }
} elseif (isset($_POST['reset'])) {
  $email = $_POST['email']; 
  $resetcode = quickRandom(16);
  if (fUser::reset(array(
  'email' => $email,
  'resetcode' => $resetcode,
    )
  ))
  {
    header("Location: /");
  } else {
    header("Location: /reset");
  }
} elseif (isset($_POST['resetpass'])) {
  session_start();
  $password = $_POST['password']; 
  $confirmpassword = $_POST['confirmpassword'];
  $resetcode = $_POST['resetcode'];
  if (strlen($password) < 6) {
    $_SESSION['reset'] = "Votre mot de passe doit avoir au moins 6 caratères.";
    header("Location: /reset/$resetcode");
  } elseif ($password !== $confirmpassword) {
    $_SESSION['reset'] = "Les mots de passes ne se correspondent pas.";
    header("Location: /reset/$resetcode");
  } else {
    $user = fUser::where('resetcode', '=', $resetcode)[0];
    if (isset($user)) {
      if (fUser::changepass(array(
      'username' => $user['username'],
      'password' => md5($password),
      )))
      {
        fUser::resetresetcode($user['username']);
        $_SESSION['global'] = "Votre mot de passe a été changé avec succèss!";
        header("Location: /");
      } else {
        $_SESSION['reset'] = "Une erreur s'est produite lors de la réinitialisation.";
        header("Location: /reset/$resetcode");
      }
    } else {
      $_SESSION['reset'] = "Le code de réinitialisation est invalide.";
      header("Location: /reset");
    }
  }
} elseif (isset($_POST['changepass'])) {
  session_start();
  $password = $_POST['password']; 
  $confirmpassword = $_POST['confirmpassword'];
  if (strlen($password) < 6) {
    $_SESSION['changepass'] = "Votre mot de passe doit avoir au moins 6 caratères.";
    header("Location: /user-changepass");
  } elseif ($password !== $confirmpassword) {
    $_SESSION['changepass'] = "Les mots de passes ne se correspondent pas.";
    header("Location: /user-changepass");
  } else {
    if (fUser::changepass(array(
    'username' => $_SESSION['username'],
    'password' => md5($password),
    )))
    {
      header("Location: /user-changepass");
      $_SESSION['changepass'] = "Votre mot de passe a été changé avec succèss!";
    } else {
      $_SESSION['changepass'] = "Une erreur est survenue lors du changement du mot de passe.";
      header("Location: /user-changepass");
    }
  }
} elseif (isset($_POST['logout'])) {
	fUser::logout();
	header("Location: /");
} elseif (isset($_POST['register'])) {
  $username = $_POST['username'];
  $password = $_POST['password'];
  $confirmpassword = $_POST['confirmpassword'];
  $email = $_POST['email'];
  if (strlen($username) == 0 OR strlen($password) == 0 OR strlen($email) == 0) {
  echo 'Please fill the blank.';
  } elseif (fUser::checkuser($username)) {
  echo 'Username already exist.';
  } elseif (strlen($password) < 6) {
  echo 'Password must be at least six characters long.';
  } elseif ($password !== $confirmpassword) {
  echo 'Password must match.';
  } elseif (strpos($email,'@edu.csdm.qc.ca') === false and strpos($email,'@csdm.qc.ca') === false) {
  echo 'Please enter a valid email.';
  } elseif (fUser::checkemail($email)) {
    echo 'Email already exist.';
  } else {
    $password = md5($password);
    $code = quickRandom(16);
    try {
      if (fUser::userRegister(array(
      	'username' => $username,
      	'password' => $password,
      	'email' => $email,
        'joindate' => time(),
        'code' => $code,
        'active' => 0,
      	)
      ))
      {
      	header("Location: /");
      }
    }
    catch (Exception $e) {
    	echo $e->getMessage();
    }
  header("Location: /");
  }
} else {
  header("Location :/");
}