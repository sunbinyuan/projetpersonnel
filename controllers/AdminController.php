<?php
require_once dirname(__FILE__) . "/../models/models.php";
header('Content-Type: text/html; charset=utf-8');

session_start();

if (fUser::isAdmin($_SESSION['id'])) {
  if (isset($_POST['userdelete'])) {
    if(fBlog::remove($_POST['id']) && fUser::userDelete($_POST['id'])) {
      header("Location: /user-admin");
    } else {
      echo "Une erreur s'est produite. Veuillez réssayer plus tard.";
    } 
  } elseif(isset($_POST['usermodify'])) {
    $id = $_POST['id'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    //md5 password
    $password = ((!empty($password))?md5($password):null);
    $email = $_POST['email'];
    $active = $_POST['active'];
    $admin = $_POST['admin'];
    if (
      fUser::userModify(
        array(
          'id' => $id,
          'username' => $username,
          'password' => $password,
          'email' => $email,
          'active' => $active,
          'admin' => $admin,
        )
      )
    ) {
      header("Location: /user-admin");
    } else {
      echo "Une erreur s'est produite. Veuillez réssayer plus tard.";
    }
  } elseif (isset($_GET['blogDelete'])) {
    if(fBlog::remove($_GET['blogDelete'])) {
      header("Location: /user-admin");
    } else {
      echo "Une erreur s'est produite. Veuillez réssayer plus tard.";
    } 
  } else {
    header("Location: /user-admin");
  }

} else {
  header("Location: /");
}