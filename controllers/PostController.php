<?php
require_once dirname(__FILE__) . "/../models/models.php";
header('Content-Type: text/html; charset=utf-8');

session_start();

if (isset($_POST['post'])) {
  $title = $_POST['title'];
  $content = $_POST['content'];

  // tags separated by vertical bar
  $strip_tags = "script";

  // Regex is loose and works for closing/opening tags across multiple lines and
  // is case-insensitive

  $content = preg_replace("#<\s*\/?(".$strip_tags.")\s*[^>]*?>#im", '', $content);
  $title = strip_tags($title);
  
  $blogid = fBlog::getBlogByUser($_SESSION['id']);
  try {
    if (fPost::save(
    	array(
    		'title' => $title,
    		'content' => $content,
    		'blogid' => $blogid
    	)
    ))
    {
    	header("Location: /blog/{$blogid}");
    }    
	} catch (Exception $e) {
  	echo $e->getMessage();
  }
} elseif (isset($_POST['delete'])) {
  $blogid = fBlog::getBlogByUser($_SESSION['id']);
	fPost::remove(
		array(
		'id' => $_POST['id'],
		'blogid' => $blogid
		)
	);
  header("Location: /blog/{$blogid}");
} elseif (isset($_POST['edit'])) {
  $blogid = fPost::getBlogByPost($_POST['id']);  
  $userid = fBlog::getUserbyBlog($blogid);

  if ($_SESSION['id'] == $userid || fUser::isAdmin($_SESSION['id'])) {
    $content = $_POST['content'];

    // tags separated by vertical bar
    $strip_tags = "script";

    // Regex is loose and works for closing/opening tags across multiple lines and
    // is case-insensitive

    $content = preg_replace("#<\s*\/?(".$strip_tags.")\s*[^>]*?>#im", '', $content);
    $title = strip_tags($_POST['title']);
    try {
      if (fPost::save(
      	array(
      		'title' => $title,
      		'content' => $content,
      		'postid' => $_POST['id'],
          'publishdate' => $_POST['publishdate'],
      	)
      ))
      {
      	header("Location: /blog/{$blogid}");
      }    
  	} catch (Exception $e) {
    	echo $e->getMessage();
    }
  } else {
    echo 'Veuillez vous connecter pour performer cette action.';
  }
} else {
  header("Location :/");
}