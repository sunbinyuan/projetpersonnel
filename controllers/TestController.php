<?php

/**
 * Merge two dimensional arrays my way
 *
 * Will merge keys even if they are of type int
 *
 * @param  array $array1 Initial array to merge.
 * @param  array ...     Variable list of arrays to recursively merge.
 *
 * @author Erik Pettersson <mail@ptz0n.se>
 */
function array_merge_myway()
{
    $output = array();

    foreach(func_get_args() as $array) {
        foreach($array as $key => $value) {
            $output[$key] = isset($output[$key]) ?
                array_merge($output[$key], $value) : $value;
        }
    }

    return $output;
}

/**
 * Version of array_merge_recursive without overwriting numeric keys
 *
 * @param  array $array1 Initial array to merge.
 * @param  array ...     Variable list of arrays to recursively merge.
 *
 * @link   http://www.php.net/manual/en/function.array-merge-recursive.php#106985
 * @author Martyniuk Vasyl <martyniuk.vasyl@gmail.com>
 */
function array_merge_recursive_new()
{
    $arrays = func_get_args();
    $base = array_shift($arrays);

    foreach($arrays as $array) {
        reset($base);
        while(list($key, $value) = @each($array)) {
            if(is_array($value) && @is_array($base[$key])) {
                $base[$key] = array_merge_recursive_new($base[$key], $value);
            }
            else {
                $base[$key] = $value;
            }
        }
    }

    return $base;
}

/**
 * Marge arrays recursively and distinct
 * 
 * Merges any number of arrays / parameters recursively, replacing 
 * entries with string keys with values from latter arrays. 
 * If the entry or the next value to be assigned is an array, then it 
 * automagically treats both arguments as an array.
 * Numeric entries are appended, not replaced, but only if they are 
 * unique
 *
 * @param  array $array1 Initial array to merge.
 * @param  array ...     Variable list of arrays to recursively merge.
 *
 * @link   http://www.php.net/manual/en/function.array-merge-recursive.php#96201
 * @author Mark Roduner <mark.roduner@gmail.com>
 */
function array_merge_recursive_distinct()
{
    $arrays = func_get_args();
    $base = array_shift($arrays);

    if(!is_array($base)) $base = empty($base) ? array() : array($base);

    foreach($arrays as $append) {
        if(!is_array($append)) $append = array($append);
        foreach($append as $key => $value) {
            if(!array_key_exists($key, $base) and !is_numeric($key)) {
                $base[$key] = $append[$key];
                continue;
            }
            if(is_array($value) or is_array($base[$key])) {
                $base[$key] = array_merge_recursive_distinct($base[$key], $append[$key]);
            }
            else if(is_numeric($key))
            {
                if(!in_array($value, $base)) $base[] = $value;
            }
            else {
                $base[$key] = $value;
            }
        }
    }

    return $base;
}

  $first = array('enabled' => $_POST['enabled']);
  $second = array('displayorder' => $_POST['displayorder']);


/**
 * Test functions
 *
 */
echo '<pre>';

// Recursive array merge will not work as expected
echo 'array_merge_recursive fails: ';
print_r(array_merge_recursive($first, $second));

// Recursive array merge will not work as expected
echo 'array_replace_recursive fails: ';
print_r(array_replace_recursive($first, $second));

// Let's try our function out
echo "\n", 'array_merge_myway success: ';
print_r(array_merge_myway($first, $second));

// Let's try another one
echo "\n", 'array_merge_recursive_new works too: ';
print_r(array_merge_recursive_new($first, $second));

// Let's try another one
echo "\n", 'array_merge_recursive_distinct also works: ';
print_r(array_merge_recursive_distinct($first, $second));
