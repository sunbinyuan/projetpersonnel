<?php
require_once dirname(__FILE__) . "/../models/models.php";
header('Content-Type: text/html; charset=utf-8');


session_start();

if (isset($_POST['add'])) {
  if (fWidget::count($_SESSION['id']) >= 5) {
    echo "Vous avez dépassé le nombre maximal de widgets.";
  } else {
    try {
      if (fWidget::add($_SESSION['id'])) {
        header("Location: /user-widget-id/" . fWidget::connect()->lastInsertId());
      }
    } catch(Exception $e) {
      echo $e->getMessage();
    }
  }
} elseif (isset($_POST['delete'])) {
  $array = array();
  $delete = array();
  foreach ($_POST['enabled'] as $key => $value) {
    if ($value == 1) {
      array_push($delete, $key);
    }
  }
  $array['id'] = $delete;
  $array['userid'] = $_SESSION['id'];
  try {
    fWidget::remove($array);
    header("Location: /user-widget");
  } catch (Exception $e) {
      echo $e->getMessage();
  }
} elseif (isset($_POST['save'])) {
  foreach ($_POST['enabled'] as $key => $value) {
    $enabled[$key] = array('enabled' => $value);
    # code...
  }
  foreach ($_POST['displayorder'] as $key => $value) {
    $displayorder[$key] = array('displayorder' => $value);
    # code...
  }
  $array['id'] = array_replace_recursive($enabled, $displayorder);
  $array['userid'] = $_SESSION['id'];
  if (fWidget::save($array) == true)  {
    header("Location: /user-widget");
  } 
} elseif (isset($_POST['modify'])) {
    $name = strip_tags($_POST['name']);
    $content = $_POST['content'];
    try {
      if (fWidget::modify(
        array(
        'id' => $_POST['id'],
        'userid' => $_SESSION['id'],
        'name' => $name,
        'content' => $content,
        )
      )) 
      {
        header("Location: /user-widget");
      }
    } catch(Exception $e) {
      echo $e->getMessage();
    }
} else {
  header("Location :/");
}